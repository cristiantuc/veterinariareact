import React,{useState} from 'react';

function Turnos() {
  const [mascota, setMascota] = useState('');
  const [duenx, setDuenx] = useState('');
  let turnos = (JSON.parse(localStorage.getItem('turnosJSON')) || []);
  
  const guardarTurno = (e) => {
    e.preventDefault();
    if (mascota === '') {
      alert('Ingresar nombre de mascota');
      document.getElementById('inputMascota').focus();
      return;
    }
    if (duenx === '') {
      alert('Ingrese el nombre de dueñ@')
      document.getElementById('inputDuenx');
      return;
    }
    setMascota(mascota);
    setDuenx(duenx);
    turnos = [{mascota, duenx}, ...turnos];
    localStorage.setItem('turnosJSON', JSON.stringify(turnos));
    console.log(turnos);
    document.getElementById('inputMascota').value = '';
    document.getElementById('inputDuenx').value = '';
    document.getElementById('inputMascota').focus();
  };

  return(
    <div>
      <h2 className="App">Solicitar turno</h2>
      <form className="px-5 pt-4 mb-4" onSubmit={guardarTurno}>
        <div className="form-group">
          <label htmlFor="inputMascota">
            <strong>
              Nombre mascota
            </strong>
          </label>
          <input 
            type="text" 
            className="form-control col-3 border-secondary" 
            id="inputMascota" 
            placeholder="Ingrese nombre mascota"
            value={mascota}
            onChange={(e)=>setMascota(e.target.value)}
            autoFocus
          />
        </div>
        <div className="form-group">
          <label htmlFor="inputDuenx">
            <strong>
              Nombre dueñ@
            </strong>
          </label>
          <input 
            type="text" 
            className="form-control col-3 border-secondary" 
            id="inputDuenx" 
            placeholder="Ingrese nombre dueñ@"
            value={duenx}
            onChange={(e)=>setDuenx(e.target.value)}
          />
          <input 
            type="submit" 
            value="Guardar"
            className="btn btn-secondary col-2 mt-3"
          />
        </div>
      </form>
      <hr 
        className="ml-5 col-11 bg-secondary"
      />
      {(turnos.lenght === 0) ? 
        <div>
          <table className="table table-bordered table-hover col-5 mt-4 ml-5">
            <thead className="bg-secondary">
              <tr>
                <th scope="col-1" className="text-white">#</th>
                <th scope="col-2" className="text-white">Mascota</th>
                <th scope="col-2" className="text-white">Duen@</th>
              </tr>
            </thead>
            <tbody>
              {turnos.map((item, index) => (
                <tr key={index}>
                  <th className="border border-secondary" scope="row">{index + 1}</th>
                  <td className="border border-secondary" key={item.mascota}>{item.mascota}</td>
                   <td className="border border-secondary" key={item.duenx}>{item.duenx}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        :
        <h2 className="text-center" >No hay turnos reservados</h2>
      }
    </div>
  );
}

export default Turnos;