import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import Inicio from './components/Inicio';
import Contacto from './components/Contacto';
import Turnos from './components/Turnos';

import './App.css';

function App() {
  return (    
    <Router>
      <nav className="navbar navbar-light bg-secondary d-flex justify-content-start">
      <Link to="/">
          <span 
            className="navbar-brand text-white ml-2 my-0 h1 py-0 pr-3 border-right"
            >Inicio
          </span>
        </Link>
        <Link to="/turnos">
          <span className="navbar-brand text-white my-0 h1 py-0 pr-3 border-right"
            >Turnos
          </span>
        </Link>
        <Link to="/contacto">
          <span 
            className="navbar-brand text-white my-0 h1"
            >Contacto
          </span>
        </Link>
      </nav>
      <Switch>
        <Route path="/turnos" component={Turnos}/>
        <Route path="/contacto" component={Contacto}/>
        <Route path="/" component={Inicio}/>
      </Switch>
    </Router>
  );
}

export default App;
